import System.Environment

compterLignes :: String -> Int
compterLignes str = length (lines str)

compterMots :: String -> Int
compterMots str = length (words str)

compterCaractere :: String -> Int
compterCaractere str = length str

compterCaractereNonVides :: String -> Int
compterCaractereNonVides str = length (concat (words str))

main :: IO ()
main = do
    args <- getArgs
    case args of 
        [filename]  ->  do
            content <- readFile filename
            print (compterLignes content)
            print (compterMots content)
            print (compterCaractere content)
            print (compterCaractereNonVides content)
        _ -> putStrLn "usage : <file>" 