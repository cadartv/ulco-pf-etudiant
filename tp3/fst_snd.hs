myFst :: (a,b) -> a
myFst (x,_) = x

mySdn :: (a,b) -> b
mySdn (_,y) = y
    
myFst3 :: (a,b,c) -> a
myFst3 (x,_,_) = x


main :: IO ()
main = do
    print (myFst ("foo", 1)) 
    print (mySdn ("foo", 1)) 
    print (myFst3 ("foo", 1, 1)) 