import System.Environment

cowsay :: String -> String
cowsay msg =    
    "  " ++ replicate n '-' ++ "\n" ++
    "< " ++ msg ++ " >\n" ++
    "  " ++ replicate n '-' ++ "\n" ++
    "  \\     ^__^ \n\ 
    \   \\    (oo)\\_______ \n\
    \        (__)\\       )\\/\\ \n\
    \            ||----w | \n\
    \            ||     ||"
    where n = length msg

main :: IO ()
main = do
    args <- getArgs
    let msg = unwords args
    putStrLn (cowsay msg) 