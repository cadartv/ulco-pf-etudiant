fuzzyLength :: [a] -> String
fuzzyLength [] = "Empty"
fuzzyLength [a] = "One"
fuzzyLength [a, b] = "Two"
fuzzyLength _ = "Many"

main :: IO ()
main = do
    putStrLn (fuzzyLength [])
    putStrLn (fuzzyLength [1])
    putStrLn (fuzzyLength [1,2])
    putStrLn (fuzzyLength [1..42])