twice :: [a] -> [a]
twice val = val ++ val


sym :: [a] -> [a]
sym val = val ++ reverse val

main :: IO ()
main = do
    print (twice [1,2])
    print (twice "to")
    print (sym [1,2])
    print (sym "to")