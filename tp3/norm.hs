dot :: [Double] -> [Double] -> Double
dot x y = sum $ zipWith (*) x y

norm :: [Double] -> Double
norm xs = sqrt (dot xs xs)

main :: IO ()
main = do 
    print (dot [3,4] [1,2])
    print (dot [3,4] [3,4])
    print (norm [3,4])