--import System.Random

myword :: [String] -> Int
myword [] = 0
myword xs = length xs

myhideword :: Int -> String
myhideword x = x * '?' 

--myrand :: Float -> Int
--myrand x = 2 * x

checkchar :: Char -> String -> Bool
checkchar c s = any(\x -> x == c) s 

game :: Int -> IO String
game 0 = return "perdu"
game n = do 
            putStr "> "
            letter <- getChar
            if checkchar letter "function" == false then
                if n == 0 then 
                    putStrLn "                \n\
                             \                \n\
                             \                \n\
                             \                \n\
                             \                \n\
                             \                \n\
                             \                "  
                else 
                    if n == 1 then 
                    putStrLn "                \n\
                             \                \n\
                             \                \n\
                             \                \n\
                             \                \n\
                             \                \n\
                             \_______________" 
                else 
                    if n == 2 then 
                    putStrLn "               \n\
                            \    |           \n\
                            \    |           \n\
                            \    |           \n\
                            \    |           \n\
                            \    |           \n\
                            \____|___________" 
                else 
                    if n == 3 then 
                    putStrLn "     _______    \n\
                            \    |/           \n\
                            \    |            \n\
                            \    |            \n\
                            \    |            \n\
                            \    |            \n\
                            \____|___________" 
                else 
                    if n == 4 then 
                    putStrLn "     _______    \n\
                            \    |/      |    \n\
                            \    |            \n\
                            \    |            \n\
                            \    |            \n\
                            \    |            \n\
                            \____|___________" 
                else 
                    if n == 5 then 
                    putStrLn "     _______    \n\
                            \    |/      |    \n\
                            \    |      (_)   \n\
                            \    |      \|/   \n\
                            \    |       |    \n\
                            \    |      / \   \n\
                            \____|___________"        
                game (n-1)

main :: IO ()
main = do 
    putStrLn "+-------+\n\
             \| hello |\n\
             \+-------+"
    word <- myword ["function", "functional", "programming", "haskell", "list", "pattern matching", "recursion", "tuple", "type system"]      
    hword <- myhideword word
    putStrLn hword
    res <- game 6 
    putStrLn res
