myRangeTuple1 :: (Int,Int) -> [Int]
myRangeTuple1 (a,b) = [a..b]

myRangeTuple2 :: Int -> [Int]
myRangeTuple2 a = myRangeTuple1(0, a)

myRangeCurry1 :: Int -> Int -> [Int]
myRangeCurry1 a b = [a..b]

myRangeCurry2 :: Int -> [Int]
myRangeCurry2 a = myRangeCurry1 0 a

myCurry :: ((Int, Int) -> [Int]) -> Int -> Int -> [Int]
myCurry f1 a b = f1 (a, b)

main :: IO ()
main = do
    print (myRangeTuple1 (0,9))
    print (myRangeTuple2 9)
    print (myRangeCurry1 0 9)
    print (myRangeCurry2 9)
    print (myCurry myRangeTuple1 0 9)