{-
add42 :: Int -> Int
add42 x = x + 42

estPositif :: Int -> Bool
estPositif x = x > 0

plus42Positif :: Int -> Bool
plus42Positif = estPositif . add42
-}

plus42Positif :: Int -> Bool
plus42Positif = (>0) . (+42)

mul2Plus1 :: Int -> Int
mul2Plus1 = (+1) . (*2)

mul2Moins1 :: Int -> Int
mul2Moins1 = (flip (-) 1) . (*2)

applyTwice :: (a -> a) -> a -> a
applyTwice f = f . f

main :: IO ()
main = do
    putStrLn "composition"
    print (plus42Positif 2)
    print (plus42Positif (-82))
    print (mul2Plus1 10)
    print (mul2Moins1 10)
    print (applyTwice mul2Plus1 10)