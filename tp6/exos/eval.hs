myTake2 :: [a] -> [a]
myTake2 = take 2

myGet2 :: [Int] -> Int
myGet2 = (!! 2)

main :: IO ()
main = do
    print (myTake2 [1..4])
    print (myGet2 [1..4])
    print (map (2*) [1..4])
    print (map ((*) 2) [1..4])
    print (map (*2) [1..4])
    print (map ((flip (*)) 2) [1..4])
    print (map (`div` 2) [1..4])
    print (map ((flip div) 2) [1..4])
