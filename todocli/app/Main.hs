import Text.Read
import System.Environment
import System.IO

type Task = (Int, Bool, String)
type Todo = (Int, [Task])

-- Gestion du menu
{- menu :: Int -> IO String
menu 0 = return "exit"
menu n = do 
    putStr "> "
    str <- getLine
    if null str then return "empty line"
        else do
        -- gestion des cas a faire un case
        if str == "doc" then do 
            resdoc <- doc 1   
            putStrLn resdoc
        else do
            if str == "print" then do 
                resaff_print <- aff_print 1   
                putStrLn resaff_print
            else do
                menu n -}

-- Gestion documentation
doc :: Int -> IO String
doc 0 = return "exit"
doc n = do
    putStrLn "usage :"
    putStrLn "  print"
    putStrLn "  print todo"
    putStrLn "  print done"
    putStrLn "  add <string>"
    putStrLn "  do <int>"
    putStrLn "  undo <int>"
    putStrLn "  del <int>"
    putStrLn "  exit"
    putStr "> "
    str <- getLine
    if null str then return "empty line"
    else do 
        if str == "exit" then doc (n-1) else do doc n

-- Gestion print 
aff_print :: Int -> IO String
aff_print 0 = return "exit"
aff_print n = do
    putStr "> "
    str <- getLine
    if null str then return "empty line"
    else do 
        contents <- readFile "tasks.txt"
        let todo1 = read $! contents :: Todo
        mapM_ print (snd todo1)
        aff_print (n-1)

--Gestion add
aff_add :: Int -> IO String
aff_add 0 = return "exit"
aff_add n = do
    putStr "> "
    str <- getLine
    if null str then return "empty line"
    else do 
        contents <- readFile "tasks.txt"
        let todo1 = read $! contents :: Todo
        let (nextId, tasks) = todo1
        let newid = nextId + 1 
        let add_contents = read $ show newid ++ ", False," ++ str
        writeFile "tasks.txt" add_contents
        aff_add (n-1)

main :: IO ()
main = do
    putStrLn ">> Documentation :"
    res <- doc 1   
    putStrLn res
    putStrLn ">> Print :"
    resaff_print <- aff_print 1   
    putStrLn resaff_print
    {- putStrLn ">> Add :"
    resaff_add <- aff_add 1   
    putStrLn resaff_add -}
    


