main :: IO()
main = do
    putStr "Name? "
    name <- getLine
    if name == ""
        then putStrLn ("Goodbye !") 
        else do
            putStrLn ("Hello " ++ name ++ " !") 
            main
            