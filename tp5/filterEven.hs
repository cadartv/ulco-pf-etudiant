import Data.Char (isLetter)

filterEven1 :: [Int] -> [Int]
filterEven1 [] = []
filterEven1 (x:xs) =   if even x 
                        then x:filterEven1 xs
                        else filterEven1 xs

filterEven2 :: [Int] -> [Int]
filterEven2 xs = filter even xs

myfilter :: (a -> Bool) -> [a] -> [a]
myfilter _ [] = []
myfilter p (x:xs) =   if p x 
                        then x : myfilter p xs
                        else myfilter p xs

main :: IO ()
main = do
    print (filterEven1 [1..4])
    print (filterEven2 [1..4])
    print (myfilter even [1..4])
    print (myfilter isLetter "aeazeaezaefvaeve35435353")
