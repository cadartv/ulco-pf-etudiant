foldSum1 :: [Int] -> Int
foldSum1 [] = 0
foldSum1 (x:xs) = x + foldSum1 xs

foldSum2 :: [Int] -> Int
foldSum2 list = foldl (+) 0 list

myfold :: (b -> a -> b) -> b -> [a] -> b 
myfold _ acc [] = acc
myfold f acc (x:xs) = myfold f (f acc x) xs

main :: IO ()
main = do
    print (foldSum1 [1..4])
    print (foldSum2 [1..4])
    print (myfold (+) 0 [1..4])
    print (myfold () "foobar")