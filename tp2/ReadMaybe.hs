import Text.Read

main :: IO ()
main = do
    line <- getLine
    let x = readMaybe line
    print (x:: Maybe Int)
    putStrLn "this is the end"