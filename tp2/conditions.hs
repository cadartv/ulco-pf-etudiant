formaterParite :: Int -> String
formaterParite n =  
    if even n 
        then "pair"
        else "impair"

formaterSigne :: Int -> String
formaterSigne n =
    if n == 0 
    then "nul"
    else if n > 0 
        then "positif"
        else "negatif"

formaterPariteGarde :: Int -> String
formaterPariteGarde n 
    | even n = "pair"
    | otherwise = "impair"

formaterSigneGarde :: Int -> String
formaterSigneGarde n
    | n == 0 = "null"
    | n < 0 = "negatif"
    | otherwise = "positif"

main :: IO ()
main = do
    putStrLn (formaterParite 21)
    putStrLn (formaterSigne 24)
    {- 
    putStrLn (formaterSigne 0)
    putStrLn (formaterSigne (-24))
    putStrLn (formaterSigne 24)
    -}
    putStrLn (formaterPariteGarde 21)
    putStrLn (formaterSigneGarde 24)
    {- 
    putStrLn (formaterSigneGarde 0)
    putStrLn (formaterSigneGarde (-24))
    putStrLn (formaterSigneGarde 24)
    -}