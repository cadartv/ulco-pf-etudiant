xor1 :: Bool -> Bool -> Bool
xor1 a b = (a && not b) || (not a && b)

xor2 :: Bool -> Bool -> Bool
xor2 a b = if a then not b else b

xor3 :: Bool -> Bool -> Bool
xor3 a b = case (a,b) of
                (True, True) -> False
                (False, False) -> False
                _ -> True
              
xor4 :: Bool -> Bool -> Bool                
xor4 a b 
    | a == b = False
    |otherwise = True
    
xor5 :: Bool -> Bool -> Bool                
xor5 True True = False 
xor5 False False = False
xor5 _ _ = True

testXor :: (Bool -> Bool -> Bool) -> Bool
testXor f 
    = True   `f` True == False
    && False `f` False == False
    && True  `f` False == True
    && False `f` True == True
    
main :: IO ()
main = do
    putStrLn "\nxor1"
    print (True 'xor1' True)
    print (True 'xor1' False)
    print (False 'xor1' True)
    print (False 'xor1' False)

    putStrLn "\nxor2"
    print (True 'xor2' True)
    print (True 'xor2' False)
    print (False 'xor2' True)
    print (False 'xor2' False)

    putStrLn "\nxor3"
    print (True 'xor3' True)
    print (True 'xor3' False)
    print (False 'xor3' True)
    print (False 'xor3' False)

    putStrLn "\nxor4"
    print (True 'xor4' True)
    print (True 'xor4' False)
    print (False 'xor4' True)
    print (False 'xor4' False)

    putStrLn "\nxor5"
    print (True 'xor5' True)
    print (True 'xor5' False)
    print (False 'xor5' True)
    print (False 'xor5' False)