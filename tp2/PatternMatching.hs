formateNul1 :: Int -> String
formateNul1 x = show x ++ " est " ++ res
    where  res = case x of
                        0 -> "nul"
                        _ -> "non nul"

formateNul2 :: Int -> String
formateNul2 x = show x ++ " est " ++ fmt x
    where fmt 0 = "nul"
          fmt _ = "non nul"

main :: IO ()
main = do
    putStrLn (formateNul1 0)
    putStrLn (formateNul1 42)
    putStrLn (formateNul2 0)
    putStrLn (formateNul2 42)