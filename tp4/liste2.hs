mulListe :: Int -> [Int] -> [Int]
mulListe _ [] = []
mulListe n (x : xs) = (n * x) : (mulListe n xs)

selectListe :: Ord a => (a, a) -> [a] -> [a]
selectListe _ [] = []
selectListe (xMin, xMax) (x:xs) =
    if x <= xMax && x >= xMin
        then x : selectListe (xMin, xMax) xs
    else selectListe (xMin, xMax) xs

sumListe :: Num a => [a] -> a
sumListe [] = 0
sumListe (x:xs) = x + sumListe xs

main :: IO ()
main = do
        print (mulListe 2 [21, 3])
        print (selectListe (2, 3) [3, 4, 1])
        print (sumListe [3, 4, 1.0::Double])
        print (sumListe [3, 4, 1::Int])