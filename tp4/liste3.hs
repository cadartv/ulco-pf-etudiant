
mylast :: [a] -> a
mylast [x] = x
mylast (x:xs) = mylast xs
mylast [] = error "empty list"

myinit :: [a] -> [a]
myinit (x;_) = x
myinit [] = error "empty list"

myreplicate :: Int -> a -> [a]
myreplicate 0 x = []
myreplicate n x = n : myreplicate (n-1) x

mydrop :: Int -> [æ] -> [a]
mydrop 0 xs = xs
mydrop _ [] = []
mydrop n (x:xs) = mydrop (n-1) xs

mytake :: Int -> [a] -> [a]
mytake 0 _ = []
mytake _ [] = []
mytake n (x:xs) = x : mytake (n-1) xs

mylast :: Int -> [a] -> [a]
mylast 0 _ = []
mylast _ [] = []
mylast n (x:xs) = x : mylast (n-1) xs

main :: IO ()
main = do
    print (mylast "foobar")
    print (myinit "foobar")
    print (myreplicate 3 'a')
    print (mydrop 2 "foobar")
    print (mytake 2 "foobar")